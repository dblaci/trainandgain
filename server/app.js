const Express = require("express")();
const Http = require("http").Server(Express);
const Socketio = require("socket.io")(Http, {'origins': '*:*'});

// workaround form cors: https://stackoverflow.com/questions/24058157/socket-io-node-js-cross-origin-request-blocked
//Socketio.origins('*:*');
// Socketio.origins((origin, callback) => {
//     callback(null, true);// everything!!!!444
//     if (origin !== 'https://foo.example.com') {
//         console.log('origin not allowed: ' + origin);
//         return callback('origin not allowed: ' + origin, false);
//     }
//     callback(null, true);
// });

Socketio.set('origins', '*:*');

const SillyName = require('sillyname');

const _ = require('lodash');

const demoStartTime = 2 * 3600;

var globalState = {
    cityStart: 'Kraków',
    cityDestination: 'Warszawa',
    progress: demoStartTime,
    lengthKm: 311,
    lengthSecs: 3600 * 4.2,
    tasks: {},
};

/**
 * users by socket identifier
 *
 * {name: 'asdasd', score: 123123123}
 *
 * @type {{}}
 */
var users = {

};

var chat = [];

var taskNO = 0;

Http.listen(3000, () => {
    console.log("Listening at :3000 ...");
});

/**
 * all user in the order of score
 *
 * @returns {Array}
 */
const getUserList = () => {
    let ret = [];
    _.each(_.sortBy(users, ['score']).reverse(), (data) => {
        ret.push(data);
    });
    return ret;
};

const taskChange = () => {
    Socketio.emit('updateTasks', globalState.tasks);
};

const updateProgress = () => {
    Socketio.emit('updateProgress', globalState.progress);
};

const hasTaskType = (taskType) => {
    let found = false;
    _.each(globalState.tasks, (taskData) => {
        if (taskData.type === taskType) {
            found = true;
            return false;
        }
    });
    return found;
};

const generateRandomTask = () => {
    if (Math.random() < 0.01) {
        if (hasTaskType('fire')) {
            return;
        }
        // tűz task
        let newTask = {
            type: 'fire',
            title: 'Fire!!!',
            processUserInput: userInput => {
                if (userInput === 'extinguish') {
                    newTask.size--;
                    checkTasks();
                    taskChange();
                    return 1;
                }
                return 0;
            },
            isReady: () => {
                if (newTask.size <= 0) {
                    _.each(users, (user) => {
                        user.score += 5;
                    });
                    return true;
                }
                return false;
            },
            size: parseInt(10 + Math.random() * 20),
        };
        globalState.tasks['task' + ++taskNO] = newTask;
        taskChange();
    }
};

const addStationTask = () => {
    let stationTime = 60; // seconds
    if (hasTaskType('station')) {
        return;
    }
    let newTask = {
        type: 'station',
        title: 'Kielce',
        endTime: new Date().getTime() + stationTime * 1000,
        remainingTime: stationTime,
        processUserInput: userInput => {
            if (userInput === 'seen') {
                return 3;
            }
            return 0;
        },
        isReady: () => {
            newTask.remainingTime = parseInt((newTask.endTime - new Date().getTime()) / 1000);
            taskChange(); // TODO: not nice, station task is always changing.
            if (newTask.remainingTime <= 0) {
                return true;
            }
            return false;
        },
    };
    globalState.tasks['task' + ++taskNO] = newTask;
    taskChange();
};

const checkTasks = () => {
    let change = false;

    // check todos if valid yet
    _.each(globalState.tasks, (task, idx) => {
        if (task.isReady()) {
            delete(globalState.tasks[idx]);
            taskChange();
            change = true;
        }
    });
    if (change) {
        triggerUpdateUserList();
    }
};

/**
 * update user list for every connected user (score change, name change)
 */
const triggerUpdateUserList = () => {
    Socketio.emit('updateUserList', getUserList());
};

setInterval(() => {
    generateRandomTask();

    globalState.progress += 1; // 1 second passed

    // csalunk, ha körbeért, kezdődik előlről
    if (globalState.progress >= globalState.lengthSecs) {
        globalState.progress = 0;
    }

    checkTasks();
    updateProgress();
}, 1000);

Socketio.on("connection", socket => {
//    console.log(socket.id);
    let myUser;
    if (socket.id in users) {
        myUser = users[socket.id];
    } else {
        // init
        myUser = users[socket.id] = {
            name: SillyName(),
            score: 0,
        }
    }

    socket.on('disconnect', reason => {
        console.log(socket.id, 'disconnect', reason);
        delete users[socket.id];
    });

    socket.emit("initState", {
        globalState: globalState,
        user: users[socket.id],
        userList: getUserList(),
        chat: chat,
    });

    socket.on('changeName', newName => {
        myUser.name = newName;
        triggerUpdateUserList();
    });

    socket.on('newChatMessage', (message) => {
        let date = new Date();
        chat.push({time: date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds(),userName: users[socket.id].name, message: message});
        if (chat.length > 5) {
            chat = _.drop(chat, chat.length - 5);
        }
        Socketio.emit('chatUpdate', chat);
    });

    socket.on('processTask', (taskID, data) => {
        if (taskID in globalState.tasks) {
            let myUser = users[socket.id];
            Socketio.emit('userProcess', {
                time: new Date().getTime(),
                name: myUser.name,
                data: data,
            });
            let score = globalState.tasks[taskID].processUserInput(data);
            myUser.score += score;
            triggerUpdateUserList(myUser);
            socket.emit('updateUser', myUser);
        }
    });

    socket.on('adminSetStationMode', () => {
        addStationTask();
    });

    socket.on('adminTriggerNewInfo', () => {
        let info = [
            'Train stopped because construction is in progress ...',
            'Train stopped - we wait for another train',
            'Please close the windows, storm is aproaching',
            'Interesting sight on the left!',
        ];
        Socketio.emit('information', info[_.random(info.length - 1)]);
    });

});
